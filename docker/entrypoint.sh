#!/bin/bash
# clone baseline repo
git clone https://${GIT_USERNAME}:${GIT_PASSWORD}@bitbucket.org/kyridev/${BASELINE_REPO}.git
cd ${BASELINE_REPO}
git checkout ${BASELINE_BRANCH}
#create lowercase s3 bucket name from customer
#initiate minio bucket with baseline files
mkdir -p ../opt/ptl/baseline
cp process-templates-storage/${CUSTOMER}/${USER}/files/* ../opt/ptl/baseline

cd ../opt/ptl
#start ptló
java -jar ptl-framework.jar --pt.client.customer=${CUSTOMER} --pt.client.user=${USER} --pt.client.ip=${IP} --pt.client.port=${PORT} --pt.filter.team-name=${TEAM} --pt.filter.team-scope=${SCOPE}