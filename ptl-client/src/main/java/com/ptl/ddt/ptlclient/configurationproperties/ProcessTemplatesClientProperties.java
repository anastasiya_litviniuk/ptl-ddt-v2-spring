package com.ptl.ddt.ptlclient.configurationproperties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Getter
@Setter
@ConfigurationProperties(prefix = "pt.client")
public class ProcessTemplatesClientProperties {

    private String ip;
    private String port;
    private String customer;
    private String user;
    private String password;
    private String baseUrl;
    private String clientId;
}
