package com.ptl.ddt.ptlclient.configuration;

import com.ptl.ddt.ptlclient.client.ProcessTemplatesClient;
import com.ptl.ddt.ptlclient.configurationproperties.ProcessTemplatesClientProperties;
import io.netty.channel.ChannelOption;
import io.netty.channel.epoll.EpollChannelOption;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;
import io.netty.handler.ssl.util.InsecureTrustManagerFactory;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.client.reactive.ClientHttpConnector;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.security.oauth2.client.*;
import org.springframework.security.oauth2.client.registration.ReactiveClientRegistrationRepository;
import org.springframework.security.oauth2.client.web.reactive.function.client.ServerOAuth2AuthorizedClientExchangeFilterFunction;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.netty.http.client.HttpClient;

import javax.net.ssl.SSLException;

@EnableConfigurationProperties(ProcessTemplatesClientProperties.class)
@PropertySource("classpath:application-client.properties")
@Configuration
public class ProcessTemplatesConfiguration {

    @Bean
    ProcessTemplatesClientProperties clientProperties() {
        return new ProcessTemplatesClientProperties();
    }

    @Bean
    WebClient processTemplateWebClient(ReactiveClientRegistrationRepository clientRegistrations, ProcessTemplatesClientProperties clientProperties) throws SSLException {
        var clientService = new InMemoryReactiveOAuth2AuthorizedClientService(clientRegistrations);
        var authorizedClientManager = new AuthorizedClientServiceReactiveOAuth2AuthorizedClientManager(clientRegistrations, clientService);
        var oauth = new ServerOAuth2AuthorizedClientExchangeFilterFunction(authorizedClientManager);
        oauth.setDefaultClientRegistrationId(clientProperties.getClientId());
        //SslContext context = SslContextBuilder.forClient()
          //      .trustManager(InsecureTrustManagerFactory.INSTANCE)
            //    .build();

       // HttpClient httpClient = HttpClient.create().secure(t -> t.sslContext(context));
        HttpClient client = HttpClient.create().compress(true).wiretap(true)
                .option(ChannelOption.SO_KEEPALIVE, true);
                //.option(EpollChannelOption.TCP_KEEPIDLE, 300)
                //.option(EpollChannelOption.TCP_KEEPINTVL, 60)
                //.option(EpollChannelOption.TCP_KEEPCNT, 8);
        ClientHttpConnector connector = new ReactorClientHttpConnector(client);
        //WebClient wc = WebClient
        //        .builder()
         //       .clientConnector(new ReactorClientHttpConnector(httpClient)).build();
        return WebClient.builder()
                .baseUrl(clientProperties.getBaseUrl())
                .clientConnector(connector)
                //.clientConnector(new ReactorClientHttpConnector(HttpClient.newConnection().compress(true)))
                .codecs(configurer -> configurer.defaultCodecs().maxInMemorySize(16*1024*1024))
                .filter(oauth)
                .build();
    }

    @Bean
    ProcessTemplatesClient processTemplatesClient(WebClient webClient) {
        return new ProcessTemplatesClient(webClient);
    }
}
