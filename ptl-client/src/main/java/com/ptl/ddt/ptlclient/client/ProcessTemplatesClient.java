package com.ptl.ddt.ptlclient.client;

import com.ptl.ddt.ptlclient.client.exception.CodeNotSupportedException;
import com.ptl.ddt.ptlclient.client.exception.ResponseException;
import com.ptl.ddt.ptlcommon.template.ProcessTemplate;
import com.ptl.ddt.ptlcommon.template.ProcessTemplateStatus;
import com.ptl.ddt.ptlcommon.template.api.GetProcessTemplatesResponse;
import com.ptl.ddt.ptlcommon.template.api.RunProcessTemplateResponse;
import com.ptl.ddt.ptlcommon.template.api.StatusResponse;
import com.ptl.ddt.ptlcommon.template.api.TaskResultResponse;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.security.oauth2.client.ClientAuthorizationException;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import reactor.util.retry.Retry;

import java.time.Duration;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Function;

@Log4j2
@RequiredArgsConstructor
public class ProcessTemplatesClient {

    private static final String GET_PROCESS_TEMPLATE_STATUS_PATH = "/{id}/status";
    private static final String GET_PROCESS_TEMPLATE_RESULT_PATH = "/{code}/files";
    private static final String RUN_PROCESS_TEMPLATE_PATH = "/{code}/run";
    private static final String RUN_PRINTABLE_PROCESS_TEMPLATE_PATH = "/sync/{code}/run";
    private static final String CAN_BE_LAUNCHED_PARAM = "canBeLaunched";
    private static final String PAGE_LIMIT_PARAM = "page.limit";
    private static final String PAGE_OFFSET_PARAM = "page.offset";
    private static final int MAX_RETRY_COUNT = 1;
    private static final int RETRY_DURATION = 5;

    private final WebClient webClient;

    public RunProcessTemplateResponse runProcessTemplate(ProcessTemplate processTemplate){
        String code = processTemplate.getCode();
        try {
            log.info("Running process template {}", processTemplate.getCode());
            return getRunProcessTemplateResponse(processTemplate);
        } catch (CodeNotSupportedException e) {
            log.info("Process template: {} will be run as printable", code);
            try {
                return getRunPrintableProcessTemplateResponse(processTemplate);
            } catch (ResponseException e2){
                log.warn("Error on run printable process template {}: {}", code, e2.getMessage());
                return null;
            }
        } catch (ResponseException e1){
            log.warn("Error on run process template {}: {}", code, e1.getMessage());
            return null;
        }
    }

    @SneakyThrows
    public StatusResponse getProcessTemplateStatus(ProcessTemplate processTemplate) {
        var response = getProcessTemplateStatusResponse(processTemplate);
        for (int i = 0; i < 6; i++) {
            if (response.getStatus().equals("Complete") || response.getStatus().equals("Error") || response.getStatus().equals("Warning")) {
                return response;
            }
            Thread.sleep(20000);
        }
        return null;
    }

    public TaskResultResponse getTaskResult(String code, String UUID){
        try {
            return getTaskResultResponse(code, UUID);
        } catch (ResponseException e) {
            log.warn("Process template task {}:{} files can not be received: {}", code, UUID, e.getMessage());
            return null;
        }
    }

    public GetProcessTemplatesResponse getProcessTemplates(int limit, int offset) {
        return webClient.get()
                .uri(uriBuilder -> uriBuilder
                        .queryParam(CAN_BE_LAUNCHED_PARAM, true)
                        .queryParam(PAGE_LIMIT_PARAM, limit)
                        .queryParam(PAGE_OFFSET_PARAM, offset)
                        .build())
                .retrieve()
                .onStatus(HttpStatus::isError, handleErrorFunction())
                .bodyToMono(GetProcessTemplatesResponse.class)
                .retryWhen(defaultRetry())
                .block();
    }

    private StatusResponse getProcessTemplateStatusResponse(ProcessTemplate processTemplate) {
        return webClient.get()
                .uri(uriBuilder -> uriBuilder
                        .path(GET_PROCESS_TEMPLATE_STATUS_PATH)
                        .build(processTemplate.getUuid()))
                .retrieve()
                .onStatus(HttpStatus::isError, response -> Mono.error(new ResponseException(response.statusCode().getReasonPhrase())))
                .bodyToMono(StatusResponse.class)
                .block();
                //.filter(response -> response.getStatus().equals("Complete") || response.getStatus().equals("Error") || response.getStatus().equals("Warning"))
                //.repeatWhenEmpty(Repeat.onlyIf(r -> true)
                //       .fixedBackoff(Duration.ofSeconds(10))
                 //       .timeout(Duration.ofMinutes(5)))
    }

    private RunProcessTemplateResponse getRunProcessTemplateResponse(ProcessTemplate processTemplate) {
        return webClient.post()
                .uri(uriBuilder -> uriBuilder
                        .path(RUN_PROCESS_TEMPLATE_PATH)
                        .build(processTemplate.getCode()))
                .retrieve()
                .onStatus(HttpStatus::is5xxServerError, handleErrorFunction())
                .onStatus(status -> status.equals(HttpStatus.NOT_ACCEPTABLE), response ->
                        Mono.error(new CodeNotSupportedException(response.statusCode().getReasonPhrase())))
                .onStatus(HttpStatus::isError, handleErrorFunction())
                       // response -> {
                    //processTemplate.setStatus(ProcessTemplateStatus.FAILED);
                    //return Mono.error(new ResponseException(response.statusCode().getReasonPhrase()));
               // })
                .bodyToMono(RunProcessTemplateResponse[].class)
                .map(response -> response[0])
                //.doOnError(updateProcessTemplateAsFailed(processTemplate))
                .retryWhen(defaultRetryAndSetFailedStatus(processTemplate))
                .doOnSuccess(updateProcessTemplateAsStarted(processTemplate))
                .block();
    }

    private RunProcessTemplateResponse getRunPrintableProcessTemplateResponse(ProcessTemplate processTemplate) {
        return webClient.post()
                .uri(uriBuilder -> uriBuilder
                        .path(RUN_PRINTABLE_PROCESS_TEMPLATE_PATH)
                        .build(processTemplate.getCode()))
                .retrieve()
                .onStatus(HttpStatus::isError, response -> {
                    processTemplate.setStatus(ProcessTemplateStatus.FAILED);
                    return Mono.error(new ResponseException(response.statusCode().getReasonPhrase()));
                })
                .bodyToMono(RunProcessTemplateResponse.class)
                //.retry()
                //.doOnError(updateProcessTemplateAsFailed(processTemplate))
                .doOnSuccess(updateProcessTemplateAsStarted(processTemplate))
                .block();
    }

    private TaskResultResponse getTaskResultResponse(String code, String UUID) {
        return webClient.get()
                .uri(uriBuilder -> uriBuilder
                        .path(GET_PROCESS_TEMPLATE_RESULT_PATH)
                        .queryParam("taskId", UUID)
                        .build(code))
                .retrieve()
                .onStatus(HttpStatus::isError, response -> Mono.error(new ResponseException(response.statusCode().getReasonPhrase())))
                .toEntity(byte[].class)
                .map(responseEntity -> new TaskResultResponse(responseEntity.getBody(), Objects.requireNonNull(responseEntity.getHeaders().getContentType()).toString()))
                .retryWhen(defaultRetry())
                .block();
    }

    private static Function<ClientResponse, Mono<? extends Throwable>> handleErrorFunction() {
        return response -> Mono.error(new ResponseException(response.statusCode().getReasonPhrase()));
    }

    private static Retry defaultRetry() {
        return Retry.backoff(MAX_RETRY_COUNT, Duration.ofSeconds(RETRY_DURATION))
                .filter(throwable -> throwable instanceof ResponseException || throwable instanceof ClientAuthorizationException)
                .onRetryExhaustedThrow((retryBackoffSpec, retrySignal) -> {
                    throw new ResponseException("After max retries: " + retrySignal.failure().getMessage());
                });
    }

    private static Retry defaultRetryAndSetFailedStatus(ProcessTemplate processTemplate) {
        return Retry.backoff(MAX_RETRY_COUNT, Duration.ofSeconds(RETRY_DURATION))
                .filter(throwable -> throwable instanceof ResponseException)
                .onRetryExhaustedThrow((retryBackoffSpec, retrySignal) -> {
                    processTemplate.setStatus(ProcessTemplateStatus.FAILED);
                    throw new ResponseException("After max retries: " + retrySignal.failure().getMessage());
                });
    }

    private static Consumer<RunProcessTemplateResponse> updateProcessTemplateAsStarted(ProcessTemplate processTemplate) {
        return response -> {
            processTemplate.setStatus(ProcessTemplateStatus.STARTED);
            processTemplate.setUuid(response.getUuid());
        };
    }

    private static Consumer<? super Throwable> updateProcessTemplateAsFailed(ProcessTemplate processTemplate) {
        return response -> processTemplate.setStatus(ProcessTemplateStatus.FAILED);
    }
}