package com.ptl.ddt.ptlclient.client.exception;

public class CodeNotSupportedException extends RuntimeException {
    public CodeNotSupportedException(String message) {
        super(message);
    }
}
