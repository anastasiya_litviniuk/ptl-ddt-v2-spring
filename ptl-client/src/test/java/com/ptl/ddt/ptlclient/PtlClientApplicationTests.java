package com.ptl.ddt.ptlclient;

import com.ptl.ddt.ptlclient.client.ProcessTemplatesClient;
import com.ptl.ddt.ptlclient.configuration.ProcessTemplatesConfiguration;
import com.ptl.ddt.ptlcommon.template.api.GetProcessTemplatesResponse;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.condition.EnabledIf;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.util.ResourceUtils;

import java.io.IOException;
import java.nio.file.Files;

@AutoConfigureWebTestClient
@ActiveProfiles("test")
@ContextConfiguration(classes = ProcessTemplatesConfiguration.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class PtlClientApplicationTests {

    private static MockWebServer mockWebServer;

    @Autowired
    private ProcessTemplatesClient client;

    @BeforeAll
    static void setUp() throws IOException {
        mockWebServer = new MockWebServer();
        mockWebServer.start();
    }

    @Test
    @Disabled
    void getProcessTemplate() throws Exception {
        var mockProcessTemplateResponse = getTestProcessTemplateResponse();
        mockWebServer.enqueue(new MockResponse()
                .setBody(mockProcessTemplateResponse)
                .addHeader("Content-Type", "application/json"));
        GetProcessTemplatesResponse response = client.getProcessTemplates(5, 0);
        Assertions.assertEquals(1, response.getResults().size());
    }

    @AfterAll
    static void tearDown() throws IOException {
        mockWebServer.shutdown();
    }

    private static String getTestProcessTemplateResponse() throws IOException {
        var file = ResourceUtils.getFile("classpath:processTemplateResponseTest.json");
        return Files.readString(file.toPath());
    }

}
