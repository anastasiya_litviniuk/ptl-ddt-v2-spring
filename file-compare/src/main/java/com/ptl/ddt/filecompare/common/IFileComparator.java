package com.ptl.ddt.filecompare.common;

import com.ptl.ddt.ptlcommon.template.ProcessTemplate;

public interface IFileComparator {
    void compare(ProcessTemplate processTemplate, Boolean isOrdered);
}