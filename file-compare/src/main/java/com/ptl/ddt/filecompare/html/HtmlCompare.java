package com.ptl.ddt.filecompare.html;

import com.ptl.ddt.filecompare.common.Compare;
import com.ptl.ddt.filecompare.common.IFileComparator;
import com.ptl.ddt.filecompare.comparators.EqualsUsingRules;
import com.ptl.ddt.filecompare.reports.HtmlReportGenerator;
import com.ptl.ddt.ptlcommon.template.ProcessTemplate;
import com.ptl.ddt.ptlcommon.template.ProcessTemplateReports;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.io.FileUtils;
import org.jsoup.nodes.Document;
import org.jsoup.parser.Parser;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.asserts.SoftAssert;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.Optional;

@Log4j2
public class HtmlCompare extends Compare implements IFileComparator {

    private CompareResult result;

    public HtmlCompare(CompareResult result) {
        super();
        this.result = result;
    }

    @Override
    public void compare(ProcessTemplate processTemplate, Boolean isOrdered) {
        log.info("Comparing using HtmlCompare, obligatory order of data in report: " + isOrdered);
        processTemplate.getReportFiles().forEach(report -> {
            getHtmlDocument(report.getCurrentFile(), processTemplate).ifPresent(currentHtml -> {
                        getHtmlDocument(report.getBaselineFile(), processTemplate).ifPresent(baselineHtml -> {
                            result = compareHtmlFiles(currentHtml, baselineHtml, processTemplate.getSoftAssert(), processTemplate, isOrdered);
                            if (result.isFailed()) {
                                processTemplate.getDiffReports().add(getDiffReportFile(report, currentHtml, baselineHtml));
                            }
                        });
                    }
            );
        });
    }

    private File getDiffReportFile(ProcessTemplateReports report, Document currentHtml, Document baselineHtml) {
        HtmlReportGenerator htmlReportGenerator = new HtmlReportGenerator(currentHtml, baselineHtml);
        return htmlReportGenerator.createDiffReportFile(
                report.getCurrentFile().getFileName().toString()
        );
    }


    private Optional<Document> getHtmlDocument(Path path, ProcessTemplate processTemplate) {
        File html = path.toFile();
        Optional<Document> optionalDocument = Optional.empty();
        try {
            String htmlContent = FileUtils.readFileToString(html, StandardCharsets.UTF_8);
            optionalDocument = Optional.of(Parser.htmlParser().parseInput(htmlContent, ""));
        } catch (IOException ioException) {
            String msg = String.format("File %s does not exist.", path);
            log.warn(msg);
            processTemplate.getSoftAssert().fail(msg);
        }
        return optionalDocument;
    }

    private CompareResult compareHtmlFiles(Document currentHtml, Document baselineHtml, SoftAssert softAssert, ProcessTemplate processTemplate, boolean isOrdered) {
        return new DocumentComparator(currentHtml, baselineHtml, isOrdered).compareDocuments(softAssert, new EqualsUsingRules(processTemplate));
    }
}
