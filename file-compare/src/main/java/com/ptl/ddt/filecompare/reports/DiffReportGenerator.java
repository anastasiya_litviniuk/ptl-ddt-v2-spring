package com.ptl.ddt.filecompare.reports;

import com.github.difflib.text.DiffRow;
import lombok.extern.log4j.Log4j2;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.util.List;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicInteger;

@Log4j2
public class DiffReportGenerator extends ReportGenerator {

    private final List<DiffRow> diffRowList;

    String RED = "#ff9980";
    String GREEN = "#62F1A9";

    public DiffReportGenerator(List<DiffRow> diffRowList) {
        this.diffRowList = diffRowList;
    }

    @Override
    protected Document createDiffDocument(String fileName) {
        Document report = Jsoup.parse("<html></html>");
        AtomicInteger lineCount = new AtomicInteger();
        //default classes for diff: editOldInline, editNewInline
        report.head()
                .appendElement("link rel=\"stylesheet\" href=\"https://cdn.simplecss.org/simple.min.css\"")
                .appendElement("style")
                .append(String.format(".editOldInline { background-color:%s }", RED))
                .append(String.format(".editNewInline { background-color:%s }", GREEN));
        Element table = report
                .body().attr("style", "display:inline")
                .appendElement("table");
        table
                .attr("style", "table-layout:fixed")
                .appendElement("tr")
                .appendElement("th").attr("colspan", "3").attr("style", "text-align:center").appendText(fileName)
                .appendElement("tr")
                .appendElement("th").appendText("Line number : diff type")
                .appendElement("th").appendText("Baseline file")
                .appendElement("th").appendText("Current file")
        ;
        diffRowList.forEach(diffRow -> {
            lineCount.getAndIncrement();
            if (!diffRow.getTag().equals(DiffRow.Tag.EQUAL)) {
                table
                        .appendElement("tr")
                        .appendElement("td").appendText(lineNumAndDiffType(lineCount, diffRow))
                        .appendElement("td").attr("style", "word-wrap:break-word").append(diffRow.getOldLine())
                        .appendElement("td").attr("style", "word-wrap:break-word").append(diffRow.getNewLine());
            }
        });
        return report;
    }

    private String lineNumAndDiffType(AtomicInteger line, DiffRow diffRow) {
        return String.format("%s : %s", line.getAndIncrement(), diffRow.getTag().toString().toLowerCase(Locale.ROOT));
    }
}
