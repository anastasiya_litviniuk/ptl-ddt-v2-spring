package com.ptl.ddt.filecompare.comparators;

import com.ptl.ddt.filecompare.rules.RegexPatternCompiler;
import com.ptl.ddt.ptlcommon.template.ProcessTemplate;
import lombok.extern.log4j.Log4j2;

import java.util.function.BiPredicate;
import java.util.regex.Pattern;

@Log4j2
public class EqualsUsingRules implements BiPredicate<String, String> {

    private final ProcessTemplate processTemplate;
    private Pattern pattern;

    public EqualsUsingRules(ProcessTemplate processTemplate) {
        this.processTemplate = processTemplate;
        pattern = new RegexPatternCompiler().getPattern();
    }

    @Override
    public boolean test(String expected, String actual) {
        expected = replaceMatches(expected);
        actual = replaceMatches(actual);
        return actual.equals(expected);
    }

    private String replaceMatches(String text) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(text);
        if (!text.isBlank()) {
            return pattern.matcher(text).replaceAll("replace-regex");
        } else {
            //it is empty <td>, do nothing
            return text;
        }
    }
}
