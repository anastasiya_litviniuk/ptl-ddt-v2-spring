package com.ptl.ddt.filecompare.html;

public class CompareResult {

    private boolean result = true;

    public boolean isFailed() {
        return !result;
    }

    public void markFailed() {
        if (result) {
            this.result = false;
        }
    }

}
