package com.ptl.ddt.filecompare.reports;

import lombok.extern.log4j.Log4j2;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

@Log4j2
public class HtmlReportGenerator extends ReportGenerator {

    private final Document baseline;
    private final Document current;

    public HtmlReportGenerator(Document current, Document baseline) {
        this.current = current;
        this.baseline = baseline;
    }

    @Override
    protected Document createDiffDocument(String fileName) {
        Document report = Jsoup.parse("<html></html>");
        report.appendChild(current.head());
        report
                .body().appendElement("table")
                .appendElement("tr")
                .appendElement("th").attr("colspan", "2").attr("style", "text-align:center").appendText(fileName)
                .appendElement("tr")
                .appendElement("th").appendText("Baseline file")
                .appendElement("th").appendText("Current file")
                .appendElement("tr")
                .appendElement("td")
                .appendChild(baseline.body())
                .appendElement("td")
                .appendChild(current.body());
        return report;
    }
}
