package com.ptl.ddt.filecompare.reports;

import lombok.extern.log4j.Log4j2;
import org.apache.commons.io.FileUtils;
import org.jsoup.nodes.Document;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;

@Log4j2
public abstract class ReportGenerator {

    public File createDiffReportFile(String fileName) {
        File diffReportFile = null;
        Document document = createDiffDocument(fileName);
        try {
            //todo make 'reports' path configurable
            diffReportFile = Paths.get("reports", fileName).toFile();
            FileUtils.writeStringToFile(diffReportFile, document.html(), StandardCharsets.UTF_8);
            log.info(String.format("Finished marking diff in file: %s, file saved.", diffReportFile));
        } catch (IOException ioException) {
            log.error("Exception thrown during execution: " + ioException.getLocalizedMessage());
        }
        return diffReportFile;
    }

    protected abstract Document createDiffDocument(String fileName);
}
