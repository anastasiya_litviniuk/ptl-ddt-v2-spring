package com.ptl.ddt.filecompare.diff;

import com.github.difflib.DiffUtils;
import com.github.difflib.algorithm.DiffException;
import com.github.difflib.patch.AbstractDelta;
import com.github.difflib.patch.Patch;
import com.github.difflib.text.DiffRow;
import com.github.difflib.text.DiffRowGenerator;
import com.ptl.ddt.filecompare.common.Compare;
import com.ptl.ddt.filecompare.common.IFileComparator;
import com.ptl.ddt.filecompare.comparators.EqualsUsingRules;
import com.ptl.ddt.filecompare.html.CompareResult;
import com.ptl.ddt.filecompare.reports.DiffReportGenerator;
import com.ptl.ddt.ptlcommon.template.ProcessTemplate;
import lombok.extern.log4j.Log4j2;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Log4j2
public class DiffCompare extends Compare implements IFileComparator {

    @Override
    public void compare(ProcessTemplate processTemplate, Boolean isOrdered) {
        log.info("Comparing using DiffCompare, obligatory order of data in report: " + isOrdered);
        processTemplate.getReportFiles().forEach(report -> {
            List<String> baselineFileLines = readLines(report.getBaselineFile(), processTemplate);
            List<String> currentFileLines = readLines(report.getCurrentFile(), processTemplate);
            log.info("baseline file line count: {} ", baselineFileLines.size());
            log.info("current file line count: {} ", currentFileLines.size());
            Optional<Patch<String>> optionalPatch = createDiffPatch(baselineFileLines, currentFileLines, processTemplate);
            optionalPatch.ifPresent(patch -> {
                if (!isOrdered) {
                    log.info("Order of data does not matter, will remove all affected diffs.");
                    new DeltaComparator(patch).updatePatch();
                }
                for (AbstractDelta<String> delta : patch.getDeltas()) {
                    processTemplate.getSoftAssert().fail(delta.toString());
                }
                if (!patch.getDeltas().isEmpty()) {
                    DiffRowGenerator generator = DiffRowGenerator.create()
                            .showInlineDiffs(true)
                            .mergeOriginalRevised(false)
                            .inlineDiffByWord(false)
                            .build();
                    List<DiffRow> diffRows = new ArrayList<>();
                    try {
                        diffRows.addAll(generator.generateDiffRows(baselineFileLines, optionalPatch.get()));
                    } catch (DiffException e) {
                        e.printStackTrace();
                    }
                    processTemplate.getDiffReports().add(new DiffReportGenerator(diffRows).createDiffReportFile(report.getCurrentFile().getFileName().toString() + ".html"));
                }
            });
        });
    }

    List<String> readLines(Path path, ProcessTemplate processTemplate) {
        List<String> lines = new ArrayList<>();
        try {
            lines = Files.readAllLines(path);
        } catch (IOException e) {
            String msg = String.format("File %s does not exist.", path);
            log.warn(msg);
            processTemplate.getSoftAssert().fail(msg);
            return lines;
        }
        return lines;
    }

    Optional<Patch<String>> createDiffPatch(List<String> baseline, List<String> current, ProcessTemplate processTemplate) {
        Optional<Patch<String>> optionalPatch = Optional.empty();
        if (!baseline.isEmpty() && !current.isEmpty()) {
            try {
                optionalPatch = Optional.of(DiffUtils.diff(baseline, current, new EqualsUsingRules(processTemplate)));
            } catch (DiffException e) {
                log.error(e.getLocalizedMessage());
            }
        } else {
            log.warn("Pair of baseline and current files was not found.");
        }
        return optionalPatch;
    }
}
