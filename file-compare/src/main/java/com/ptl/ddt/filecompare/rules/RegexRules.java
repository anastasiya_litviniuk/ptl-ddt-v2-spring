package com.ptl.ddt.filecompare.rules;

import lombok.Data;

import java.util.List;

@Data
public class RegexRules {
    List<Rules> rules;

    @Data
    public class Rules {
        String rule;
        String example;
    }

}
