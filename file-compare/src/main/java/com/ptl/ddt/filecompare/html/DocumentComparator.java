package com.ptl.ddt.filecompare.html;

import lombok.extern.log4j.Log4j2;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.testng.asserts.SoftAssert;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.BiPredicate;

@Log4j2
public class DocumentComparator {

    private final boolean isOrdered;
    private Document current;
    private Document baseline;


    String RED = "#ff9980";
    String GREEN = "#62F1A9";

    private List<Element> currentToInvestigate = new ArrayList<>();
    private List<Element> baselineToInvestigate = new ArrayList<>();


    private static final String cssPath = "td:not(:has(table))";

    public DocumentComparator(Document current, Document baseline, boolean isOrdered) {
        this.isOrdered = isOrdered;
        this.current = current;
        this.baseline = baseline;
    }

    public CompareResult compareDocuments(SoftAssert softAssert, BiPredicate<String, String> biPredicate) {
        Elements currentElements = current.select(cssPath);
        Elements baselineElements = baseline.select(cssPath);
        CompareResult compareResult = new CompareResult();
        log.info("Comparing size of html elements");
        softAssert.assertEquals(currentElements.size(), baselineElements.size(), "Size of html files is not the same");
        Iterator<Element> currentIterator = currentElements.iterator();
        Iterator<Element> baselineIterator = baselineElements.iterator();
        while (currentIterator.hasNext()) {
            if (baselineIterator.hasNext()) {
                Element currentElement = currentIterator.next();
                Element baselineElement = baselineIterator.next();
                log.trace("current text: {} baseline text: {}", currentElement.text(), baselineElement.text());
                if (!biPredicate.test(currentElement.text(), baselineElement.text())) {
                    if (isOrdered) {
                        currentElement.attributes().put("bgcolor", GREEN);
                        baselineElement.attributes().put("bgcolor", RED);
                        softAssert.fail(String.format("String in current: '%s' does not match baseline: '%s'.", currentElement.text(), baselineElement.text()));
                        compareResult.markFailed();
                    } else {
                        currentToInvestigate.add(currentElement);
                        baselineToInvestigate.add(baselineElement);
                    }
                }
            } else {
                currentIterator.forEachRemaining(element -> {
                    element.attributes().put("bgcolor", GREEN);
                });
                compareResult.markFailed();
                softAssert.fail("Current file has more data then baseline");
            }
        }
        if (baselineIterator.hasNext()) {
            baselineIterator.forEachRemaining(element -> {
                element.attributes().put("bgcolor", RED);
            });
            compareResult.markFailed();
            softAssert.fail("Baseline file has more data then current");
        }
        if(!isOrdered) {
            investigateFailures(currentToInvestigate, baselineToInvestigate, compareResult, softAssert, biPredicate);
        }
        return compareResult;
    }

    /**
     * Some elements are not ordered in html reports, we loop through them again after sorting by text, to verify the diff.
     *
     * @param currentToInvestigate  current potential diff
     * @param baselineToInvestigate baseline potential diff
     * @param compareResult         result of comparison
     * @param softAssert            softAsser holding the exception for testNG
     * @param biPredicate           test method
     */
    private void investigateFailures(List<Element> currentToInvestigate, List<Element> baselineToInvestigate, CompareResult compareResult, SoftAssert softAssert, BiPredicate<String, String> biPredicate) {
        log.warn("Elements to investigate Current: {} baseline: {}", currentToInvestigate.size(), baselineToInvestigate.size());
        checkDocument(currentToInvestigate, baselineToInvestigate, compareResult, softAssert, biPredicate, GREEN);
        log.warn("Elements left to investigate Current: {} baseline: {}", currentToInvestigate.size(), baselineToInvestigate.size());
        checkDocument(baselineToInvestigate, currentToInvestigate, compareResult, softAssert, biPredicate, RED);
    }

    private void checkDocument(List<Element> toInvestigate, List<Element> toCompare, CompareResult compareResult, SoftAssert softAssert, BiPredicate<String, String> biPredicate, String color) {
        Iterator<Element> iterator = toInvestigate.iterator();
        while (iterator.hasNext()) {
            Element element = iterator.next();
            toCompare.stream().filter(elementToCompare -> biPredicate.test(elementToCompare.text(), element.text())).findAny().ifPresentOrElse(toCompare::remove, () -> {
                element.attributes().put("bgcolor", color);
                softAssert.fail(String.format("Element with text: %s was not found among  elements.", element.text()));
                compareResult.markFailed();
            });
        }
    }
}
