package com.ptl.ddt.filecompare.configuration;

import com.ptl.ddt.filecompare.diff.DiffCompare;
import com.ptl.ddt.filecompare.html.CompareResult;
import com.ptl.ddt.filecompare.html.HtmlCompare;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CompareConfiguration {

    @Bean
    CompareResult compareResult(){
        return new CompareResult();
    }

    @Bean
    DiffCompare diffCompare(){
        return new DiffCompare();
    }

    @Bean
    HtmlCompare htmlCompare(CompareResult result){
        return new HtmlCompare(result);
    }
}
