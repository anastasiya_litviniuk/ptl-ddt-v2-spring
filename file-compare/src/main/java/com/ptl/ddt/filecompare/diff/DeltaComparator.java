package com.ptl.ddt.filecompare.diff;

import com.github.difflib.patch.AbstractDelta;
import com.github.difflib.patch.Patch;
import lombok.extern.log4j.Log4j2;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Log4j2
public class DeltaComparator {

    private final Patch<String> patch;
    List<AbstractDelta<String>> sourceDeltas = new ArrayList<>();
    List<AbstractDelta<String>> targetDeltas = new ArrayList<>();

    public DeltaComparator(Patch<String> patch) {
        this.patch = patch;
    }

    public void updatePatch() {
        patch.getDeltas().forEach(delta -> {
            if (!delta.getSource().getLines().isEmpty()) {
                sourceDeltas.add(delta);
            }
            if (!delta.getTarget().getLines().isEmpty()) {
                targetDeltas.add(delta);
            }
        });
        checkSourceDelta();
        checkTargetDelta();
    }

    private void checkTargetDelta() {
        Iterator<AbstractDelta<String>> targetIterator = targetDeltas.iterator();
        while (targetIterator.hasNext()) {
            AbstractDelta<String> targetDelta = targetIterator.next();
            sourceDeltas.stream()
                    .filter(source -> source.getSource().getLines()
                            .containsAll(targetDelta.getTarget().getLines()))
                    .findAny()
                    .ifPresent(delta ->
                    {
                        log.debug("Removing: {}", delta);
                        patch.getDeltas().remove(delta);
                    });
        }
    }

    private void checkSourceDelta() {
        Iterator<AbstractDelta<String>> sourceIterator = sourceDeltas.iterator();
        while (sourceIterator.hasNext()) {
            targetDeltas.stream()
                    .filter(target -> target.getTarget().getLines()
                            .containsAll(sourceIterator.next().getSource().getLines()))
                    .findAny()
                    .ifPresent(delta -> {
                        log.debug("Removing: {}", delta);
                        patch.getDeltas().remove(delta);
                    });
        }
    }
}
