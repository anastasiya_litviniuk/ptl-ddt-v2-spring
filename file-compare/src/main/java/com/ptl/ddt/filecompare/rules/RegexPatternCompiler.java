package com.ptl.ddt.filecompare.rules;

import com.google.gson.Gson;
import lombok.extern.log4j.Log4j2;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Log4j2
public class RegexPatternCompiler {
    private static Pattern pattern = null;
    private static String rulesFile = "regex-rules.json";


    public static Pattern getPattern() {
        if (Objects.isNull(pattern)) {
            pattern = compile();
        }
        return pattern;
    }

    private static Pattern compile() {
        List<RegexRules.Rules> rules = readRulesFile();
        log.info("Number of regex patterns to compile: {}", rules.size());
        return Pattern.compile("\\b(" + rules.stream().map(RegexRules.Rules::getRule).collect(Collectors.joining("|")) + ")\\b");
    }

    private static List<RegexRules.Rules> readRulesFile() {
        InputStream inputStream = RegexPatternCompiler.class.getClassLoader().getResourceAsStream(rulesFile);
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
        return new Gson().fromJson(inputStreamReader, RegexRules.class).getRules();
    }
}
