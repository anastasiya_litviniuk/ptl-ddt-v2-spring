package com.ptl.ddt.filecompare;

import com.google.gson.Gson;
import com.ptl.ddt.filecompare.rules.RegexRules;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@SpringBootTest(classes = RulesTest.class)
public class RulesTest extends AbstractTestNGSpringContextTests {

    String rulesFile = "regex-rules.json";
    RegexRules regexRules;

    @BeforeClass
    public void setup() {
        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(rulesFile);
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
        regexRules = new Gson().fromJson(inputStreamReader, RegexRules.class);
    }

    @DataProvider
    public Object[][] rulesProvider() {
        List<RegexRules.Rules> rules = regexRules.getRules();
        String[][] ruleArray = new String[rules.size()][];
        for (var i = 0; i < rules.size(); i++) {
            ruleArray[i] = new String[]{rules.get(i).getRule(), rules.get(i).getExample()};
        }
        return ruleArray;
    }

    @Test(dataProvider = "rulesProvider")
    public void testRules(String rule, String example) {
        Matcher matcher = Pattern.compile(rule).matcher(example);
        Assert.assertTrue(matcher.find(), String.format("Can't find match using rule: %s, and example: %s", rule, example));
    }
}
