package com.ptl.ddt.filecompare;

import com.github.difflib.DiffUtils;
import com.github.difflib.algorithm.DiffException;
import com.github.difflib.patch.AbstractDelta;
import com.github.difflib.patch.Patch;
import com.ptl.ddt.filecompare.rules.RegexPatternCompiler;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

//@Log4j2
@SpringBootTest(classes = CompareTest.class)
public class CompareTest extends AbstractTestNGSpringContextTests {

    //    @Test
    public void diffCompareLib() throws DiffException, IOException {
//build simple lists of the lines of the two testfiles
        List<String> original = Files.readAllLines(new File("D:\\workspace\\ptl-ddt-v2\\baseline\\User_task_activity.UTA_PT.txt").toPath());
        List<String> revised = Files.readAllLines(new File("D:\\workspace\\ptl-ddt-v2\\current\\User_task_activity.UTA_PT.txt").toPath());
        //log.info("original file lines: {} ", original.size());
//compute the patch: this is the diffutils part
        Patch<String> patch = DiffUtils.diff(original, revised);

//simple output the computed patch to console
        SoftAssert softAssert = new SoftAssert();
        for (AbstractDelta<String> delta : patch.getDeltas()) {
            softAssert.fail(delta.toString());
        }
    }

    @Test
    public void patternTest() {
        RegexPatternCompiler.getPattern();
        RegexPatternCompiler.getPattern();
    }
}
