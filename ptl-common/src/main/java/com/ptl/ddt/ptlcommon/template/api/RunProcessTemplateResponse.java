package com.ptl.ddt.ptlcommon.template.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class RunProcessTemplateResponse {

    @JsonProperty("taskId")
    private String uuid;
    private String fileId;
    //private ProcessError error;

    //@Data
    ///public class ProcessError {
      //  private String code;
      //  private String message;
    //}
}
