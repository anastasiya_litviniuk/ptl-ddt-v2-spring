package com.ptl.ddt.ptlcommon.template;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.ptl.ddt.ptlcommon.utils.BackOffTimer;
import lombok.Data;
import org.testng.asserts.SoftAssert;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProcessTemplate {

    private String taskID;
    private String uuid;
    private String code;
    private String interfaceCode;
    private String description;
    private String module;
    private String category;
    private String type;
    private List<String> subtaskList;
    private ProcessTemplateStatus status = ProcessTemplateStatus.NEW;
    private String contentType;
    private long executionTimes;
    boolean isPrintable;
    private int counter;
    //transient to avoid serialization via gson
    @JsonIgnore
    private SoftAssert softAssert = new SoftAssert();
    @JsonIgnore
    private BackOffTimer backOffTimer = new BackOffTimer();
    @JsonIgnore
    private List<File> diffReports = new ArrayList<>();
    @JsonIgnore
    private List<ProcessTemplateReports> reportFiles = new ArrayList<>();
}
