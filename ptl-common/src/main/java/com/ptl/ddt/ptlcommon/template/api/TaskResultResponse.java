package com.ptl.ddt.ptlcommon.template.api;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TaskResultResponse {

    private byte[] data;
    private String contentType;
}
