package com.ptl.ddt.ptlcommon.template;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.With;

import java.nio.file.Path;

@Data
@With
@AllArgsConstructor
@NoArgsConstructor
public class ProcessTemplateReports {

    Path currentFile;
    Path baselineFile;
}
