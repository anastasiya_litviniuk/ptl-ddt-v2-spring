package com.ptl.ddt.ptlcommon.utils;

import lombok.extern.log4j.Log4j2;


@Log4j2
public class FileNameUtils {

    public static String normalizeFilename(String name) {
        return name.trim().replaceAll("[:\\\\/*?|<> ]", "_");
    }

    public static String getExtension(String mimeType) {
        switch (mimeType) {
            case "text/html":
                return "html";
            case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
                return "xlsx";
            case "application/vnd.ms-excel":
                return "xls";
            case "application/pdf":
                return "pdf";
            case "text/csv":
                return "csv";
            case "application/msword":
                return "rtf";
            default:
                log.info("Unknown MimeType {} , defaulting to .txt extension.", mimeType);
                return "txt";
        }
    }

}
