package com.ptl.ddt.ptlcommon.template.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class MetaData {
    private Links links;
    private int pageResults;
    private int numberOfTotalResults;
    private int pageLimit;
    private int pageOffset;

   @Data
    public class Links {
      private String current;
      private String next;
    }
}
