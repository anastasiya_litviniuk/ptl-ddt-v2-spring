package com.ptl.ddt.ptlcommon.template.api;

import lombok.Data;

import java.util.List;

@Data
public class StatusResponse {
    private String status;
    private String taskUuid;
    private String label;
    private List<StatusResponse> subTasks;
}
