package com.ptl.ddt.ptlcommon.template;

public enum ProcessTemplateStatus {
    NEW, STARTED, COMPLETED, FAILED, DOWNLOADED, FINISHED, PENDING_SUBTASK
}