package com.ptl.ddt.ptlcommon.utils;

import com.ptl.ddt.ptlcommon.template.api.StatusResponse;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class BackOffTimer {

    double basePow = 2;
    double power = 0;
    double maxPower = 5;
    double delta = 1;
    boolean increase = true;
    long delay = 1000;

    int backOffCount = 0;

    public BackOffTimer backOff(StatusResponse response) {
        return backOff(String.format("got response: %s", response.toString()));
    }

    public BackOffTimer backOff(String reason) {
        try {
            if (backOffCount > 0) {
                log.info("Waiting because: {}", reason);
                var sleepTime = Math.round(Math.pow(basePow, power) * delay);
                log.trace("Waiting for: {} seconds.", sleepTime / 1000);
                adjustPower();
                Thread.sleep(sleepTime);
            }
            backOffCount++;
        } catch (InterruptedException e) {
            log.error(e.getLocalizedMessage());
            e.printStackTrace();
        }
        return this;
    }

    public int backOffCount() {
        return backOffCount;
    }

    private void adjustPower() {
        if (increase) {
            power = power + delta;
        } else {
            power = power - delta;
        }
        if (power >= maxPower) {
            increase = false;
        }
        if (power == 0) {
            increase = true;
        }
    }


}

