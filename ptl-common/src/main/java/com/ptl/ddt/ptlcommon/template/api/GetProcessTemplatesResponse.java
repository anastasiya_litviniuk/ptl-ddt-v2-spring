package com.ptl.ddt.ptlcommon.template.api;

import com.ptl.ddt.ptlcommon.template.ProcessTemplate;
import lombok.Data;

import java.util.List;

@Data
public class GetProcessTemplatesResponse {
    private MetaData metadata;
    private List<ProcessTemplate> results;
}