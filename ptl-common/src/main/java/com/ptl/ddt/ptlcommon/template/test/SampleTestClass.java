package com.ptl.ddt.ptlcommon.template.test;

import com.ptl.ddt.ptlcommon.template.ProcessTemplate;
import io.qameta.allure.Allure;
import lombok.extern.log4j.Log4j2;
import org.testng.ITest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@Log4j2
public class SampleTestClass implements ITest {

    public static void setProcessTemplates(List<ProcessTemplate> processTemplates) {
        SampleTestClass.processTemplates = processTemplates;
    }

    private static List<ProcessTemplate> processTemplates = new ArrayList<>();
    private ThreadLocal<String> testName = new ThreadLocal<>();

    @DataProvider(parallel = true)
    public Object[][] ptProvider() {
        log.info("Size of PT list: {}", processTemplates.size());

        //
        ProcessTemplate[][] array = new ProcessTemplate[processTemplates.size()][];
        for (int i = 0; i < processTemplates.size(); i++) {
            List<ProcessTemplate> row = Collections.singletonList(processTemplates.get(i));
            array[i] = row.toArray(new ProcessTemplate[row.size()]);
        }
        return array;
    }

    @BeforeMethod
    public void BeforeMethod(Method method, Object[] testData) {
        ProcessTemplate processTemplate = (ProcessTemplate) testData[0];
        testName.set(processTemplate.getType() + "_" + processTemplate.getCode());
    }

    @Test(dataProvider = "ptProvider")
    public void compare(ProcessTemplate processTemplate) {
        Allure.getLifecycle().updateTestCase(testResult -> testResult.setName(processTemplate.getType() + "_" + processTemplate.getCode()));
        log.info("Executing test {}", processTemplate);
        processTemplate.getDiffReports().forEach(report -> {
                    log.info("Report file: {}", report.getAbsolutePath());
                    try {
                        Allure.addAttachment(report.getName(), new FileInputStream(report.getAbsolutePath()));
                    } catch (FileNotFoundException e) {
                        log.error(e.getLocalizedMessage());
                    }
                }
        );
        if (processTemplate.isPrintable()) {
            log.info("This PT is printable: {}", processTemplate.getReportFiles().get(0).getCurrentFile());
        }
        processTemplate.getSoftAssert().assertAll();
    }


    /**
     * This sets the test method name for testNG *.xml suite, used to pass results to jenkins blue ocean 'test' tab
     *
     * @return test name
     */
    @Override
    public String getTestName() {
        return testName.get();
    }
}
