package com.ptl.ddt.ptlcommon.template.test;

import com.ptl.ddt.ptlcommon.listeners.AttachFailureLogsToAllure;
import org.testng.TestNG;


public class SampleTest {

    public void createTest() {
        TestNG testng = new TestNG();
        testng.setTestClasses(new Class[]{SampleTestClass.class});
        testng.setDefaultSuiteName("ptl");
        testng.setDefaultTestName("ddt");
//        testng.addListener(new AttachFailureLogsToAllure());
        testng.run();
    }
}
