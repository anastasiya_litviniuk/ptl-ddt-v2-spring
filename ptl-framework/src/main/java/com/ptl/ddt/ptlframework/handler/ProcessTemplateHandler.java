package com.ptl.ddt.ptlframework.handler;

import com.ptl.ddt.filecompare.diff.DiffCompare;
import com.ptl.ddt.filecompare.html.HtmlCompare;
import com.ptl.ddt.ptlclient.client.ProcessTemplatesClient;
import com.ptl.ddt.ptlcommon.template.ProcessTemplate;
import com.ptl.ddt.ptlcommon.template.ProcessTemplateReports;
import com.ptl.ddt.ptlcommon.template.ProcessTemplateStatus;
import com.ptl.ddt.ptlcommon.template.api.GetProcessTemplatesResponse;
import com.ptl.ddt.ptlcommon.template.api.MetaData;
import com.ptl.ddt.ptlcommon.template.api.StatusResponse;
import com.ptl.ddt.ptlcommon.template.api.TaskResultResponse;
import lombok.extern.log4j.Log4j2;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

@Log4j2
public class ProcessTemplateHandler {

    private final ProcessTemplatesClient client;
    private final String currentPath;
    private final String baselinePath;
    private final HtmlCompare htmlCompare;
    private final DiffCompare diffCompare;

    public ProcessTemplateHandler(ProcessTemplatesClient client, String currentPath, String baselinePath, HtmlCompare htmlCompare, DiffCompare diffCompare) {
        this.client = client;
        this.currentPath = currentPath;
        createCurrentDirectory();
        this.baselinePath = baselinePath;
        this.htmlCompare = htmlCompare;
        this.diffCompare = diffCompare;
    }

    public void process(ProcessTemplate processTemplate) {
        if (processTemplate.getStatus().equals(ProcessTemplateStatus.NEW)) {
            execute(processTemplate);
        }
        if (processTemplate.getStatus().equals(ProcessTemplateStatus.STARTED)) {
            updateStatus(processTemplate);
        }
        if (processTemplate.getStatus().equals(ProcessTemplateStatus.COMPLETED)) {
            download(processTemplate);
            processTemplate.setStatus(ProcessTemplateStatus.DOWNLOADED);
        }
        if (processTemplate.getStatus().equals(ProcessTemplateStatus.DOWNLOADED)) {
            compare(processTemplate);
            processTemplate.setStatus(ProcessTemplateStatus.FINISHED);
        }
    }

    public boolean isFinished(ProcessTemplate processTemplate) {
        if (processTemplate.getStatus().equals(ProcessTemplateStatus.FINISHED)
                || processTemplate.getStatus().equals(ProcessTemplateStatus.FAILED)) {
            return true;
        } else {
            //log.info("Running PT state: " + processTemplate);
            return false;
        }
    }

    public List<ProcessTemplate> getProcessTemplates() {
        List<ProcessTemplate> processTemplates = new ArrayList<>();
        GetProcessTemplatesResponse getPTResponse = getPageLimitPTsAtOffset(500, 0);
        processTemplates.addAll(getPTResponse.getResults());
        MetaData metaData = getPTResponse.getMetadata();
        int count = 0;
        while (Objects.nonNull(metaData.getLinks().getNext()) || count > 10) {
            GetProcessTemplatesResponse response = getPageLimitPTsAtOffset(500, metaData.getPageOffset() + 500);
            processTemplates.addAll(response.getResults());
            metaData = response.getMetadata();
            count++;
        }
        return processTemplates;
    }

    private GetProcessTemplatesResponse getPageLimitPTsAtOffset(int pageLimit, int pageOffset) {
        return client.getProcessTemplates(pageLimit, pageOffset);
    }

    private void createCurrentDirectory() {
        File currentDirectory = new File(currentPath);
        if (!currentDirectory.exists()) {
            currentDirectory.mkdir();
        }
    }

    private void updateStatus(ProcessTemplate processTemplate) {
        log.info("Checking PT status of: {}", processTemplate);
        SubtaskHandler subtaskHandler = new SubtaskHandler();
        if (processTemplate.getStatus().equals(ProcessTemplateStatus.STARTED)) {
            StatusResponse statusResponse = client.getProcessTemplateStatus(processTemplate);
            if (statusResponse != null) {
                log.debug("Status for process template {}:{} is {}", processTemplate.getCode(), processTemplate.getUuid(), statusResponse.getStatus());
                switch (statusResponse.getStatus()) {
                    case "Complete":
                        if (Objects.isNull(processTemplate.getSubtaskList())) {
                            processTemplate.setSubtaskList(subtaskHandler.collectAllSubtasks(statusResponse));
                        }
                        if (subtaskHandler.checkAllSubtasksStatus(statusResponse)) {
                            processTemplate.setStatus(ProcessTemplateStatus.COMPLETED);
                            log.info("Status for process template {}:{} is {}", processTemplate.getCode(), processTemplate.getUuid(), statusResponse.getStatus());
                        }
                        break;
                    case "Error":
                        processTemplate.setStatus(ProcessTemplateStatus.FAILED);
                        log.info("Status for process template {}:{} is {}", processTemplate.getCode(), processTemplate.getUuid(), statusResponse.getStatus());
                        break;
                    case "Warning":
                        processTemplate.setStatus(ProcessTemplateStatus.FAILED);
                        log.info("Status for process template {}:{} is {}", processTemplate.getCode(), processTemplate.getUuid(), statusResponse.getStatus());
                        break;
                    default:
                    log.info("Process template {}:{} status is unknown", processTemplate.getCode(), processTemplate.getUuid());
                    //processTemplate.setStatus(ProcessTemplateStatus.FAILED);
                    break;
                }
            }
            else {
                log.warn("Process template {} status is null", processTemplate.toString());
            }
               // else{
                   // log.warn("Process template {}:{} status is unknown", processTemplate.getCode(), processTemplate.getUuid());
                    //processTemplate.setStatus(ProcessTemplateStatus.FAILED);
               // }
        }
    }

    private void execute(ProcessTemplate processTemplate) {
        log.debug("Got results from running PT - Code: {}, Status: {}", processTemplate.getCode(), processTemplate.getStatus());
        if (processTemplate.getStatus().equals(ProcessTemplateStatus.NEW)) {
            client.runProcessTemplate(processTemplate);
        }
    }

    private void download(ProcessTemplate processTemplate) {
        if (processTemplate.getStatus().equals(ProcessTemplateStatus.COMPLETED)) {
            FileHandler fileHandler = new FileHandler(processTemplate, currentPath, baselinePath);
            TaskResultResponse result = client.getTaskResult(processTemplate.getCode(), processTemplate.getUuid());
            if (result != null) {
                log.debug("Downloading results file from pt: {}", processTemplate.getCode());
                fileHandler.downloadFile(result.getData(), result.getContentType());
            }
            AtomicInteger subtaskCount = new AtomicInteger(1);
            processTemplate.getSubtaskList().forEach(subtask -> {
                TaskResultResponse subtaskResult = client.getTaskResult(processTemplate.getCode(), subtask);
                if (subtaskResult != null) {
                    log.info("Downloading {} subtask results file from pt: {}", subtaskCount.get(), processTemplate.getCode());
                    fileHandler.downloadFile(subtaskResult.getData(), subtaskResult.getContentType(), String.format("subtask-%d_", subtaskCount.getAndIncrement()));
                }
            });
        }
    }

    private void compare(ProcessTemplate processTemplate) {
        if (processTemplate.getStatus().equals(ProcessTemplateStatus.DOWNLOADED)) {
            List<ProcessTemplateReports> reports = processTemplate.getReportFiles();
            reports.forEach(report -> {
                switch (processTemplate.getContentType()) {
                    case "text/html":
                        htmlCompare.compare(processTemplate, false);
                        break;
                    default:
                        diffCompare.compare(processTemplate, false);
                        break;
                }
            });
        }
    }
}