package com.ptl.ddt.ptlframework.handler;

import com.ptl.ddt.ptlcommon.template.api.StatusResponse;

import java.util.ArrayList;
import java.util.List;


public class SubtaskHandler {
    boolean subtasksCompleted = false;
    List<String> subtasks = new ArrayList<>();

    public boolean checkAllSubtasksStatus(StatusResponse statusResponse) {
        if (statusResponse.getSubTasks().isEmpty()) {
            return true;
        } else {
            statusResponse.getSubTasks().forEach(subtask -> {
                subtasksCompleted = subtask.getStatus().equals("Complete");
                if (!subtasksCompleted) {
                    return;
                }
                checkAllSubtasksStatus(subtask);
            });
            return subtasksCompleted;
        }
    }

    public List<String> collectAllSubtasks(StatusResponse statusResponse) {
        statusResponse.getSubTasks().forEach(
                subTask -> {
                    subtasks.add(subTask.getTaskUuid());
                    collectAllSubtasks(subTask);
                }
        );
        return subtasks;
    }
}
