package com.ptl.ddt.ptlframework.configurationproperties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Getter
@Setter
@ConfigurationProperties(prefix = "pt.filter")
public class TeamScopeFilterProperties {

    private String teamName;
    private String teamScope;
}
