package com.ptl.ddt.ptlframework.actions;

import com.ptl.ddt.ptlclient.client.ProcessTemplatesClient;
import com.ptl.ddt.ptlcommon.template.ProcessTemplateStatus;
import com.ptl.ddt.ptlcommon.template.ProcessTemplate;
import com.ptl.ddt.ptlcommon.template.api.RunProcessTemplateResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;

@Log4j2
@RequiredArgsConstructor
public class TemplateExecutor {
    private final ProcessTemplatesClient client;

    public void execute(ProcessTemplate processTemplate) {
        log.debug("Executing process template: {}", processTemplate);
        if (processTemplate.getStatus().equals(ProcessTemplateStatus.NEW)) {
            RunProcessTemplateResponse runResponse = client.runProcessTemplate(processTemplate);
            if (runResponse != null) {
                processTemplate.setUuid(runResponse.getUuid());
                processTemplate.setStatus(ProcessTemplateStatus.STARTED);
            }
        }
    }
}
