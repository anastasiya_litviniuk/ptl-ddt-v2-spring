package com.ptl.ddt.ptlframework.runner;

import com.ptl.ddt.ptlcommon.template.ProcessTemplate;
import com.ptl.ddt.ptlcommon.template.test.SampleTest;
import com.ptl.ddt.ptlcommon.template.test.SampleTestClass;
import com.ptl.ddt.ptlframework.filtering.IgnoredExtensionFilter;
import com.ptl.ddt.ptlframework.filtering.TeamScopeFilter;
import com.ptl.ddt.ptlframework.handler.ProcessTemplateHandler;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;

import java.util.Date;
import java.util.List;

@Log4j2
@RequiredArgsConstructor
public class ProcessTemplateRunner {

    private final ProcessTemplateHandler processTemplateHandler;
    private final TeamScopeFilter teamScopeFilter;
    private final IgnoredExtensionFilter ignoredExtensionFilter;

    public void runProcessTemplatesLauncher() {
        parallelStream();
    }

    public void parallelStream() {
        List<ProcessTemplate> initialTemplates = processTemplateHandler.getProcessTemplates();
        log.info("Total number of PTs for current user: {}", initialTemplates.size());
        List<ProcessTemplate> processTemplates;
        if (teamScopeFilter.getTeamScope().equals("mock")) {
            log.info("Running using mock server");
            processTemplates = initialTemplates;
        } else {
            //apply team scope
            processTemplates = teamScopeFilter.getInScopeList(initialTemplates);
        }
        //apply filters
        ignoredExtensionFilter.applyFilter(processTemplates);
        //run
        if (processTemplates.size() > 0) {
            log.info("List contains {} processTemplates", processTemplates.size());
            System.setProperty("java.util.concurrent.ForkJoinPool.common.parallelism", "24");
            long start = new Date().getTime();
            boolean loopResult;
            do {
                processTemplates.parallelStream().forEach(processTemplateHandler::process);
                loopResult = processTemplates.stream().allMatch(processTemplateHandler::isFinished);
                log.info("Processing is finished: {}", loopResult);
            } while (!loopResult);
            long end = new Date().getTime();
            log.info("Execution done, it took: {} seconds. Creating test report.", (end - start) / 1000);

            SampleTestClass.setProcessTemplates(processTemplates);
            new SampleTest().createTest();
        } else {
            log.warn("API did not return any process templates for given user.");
        }
    }
}
