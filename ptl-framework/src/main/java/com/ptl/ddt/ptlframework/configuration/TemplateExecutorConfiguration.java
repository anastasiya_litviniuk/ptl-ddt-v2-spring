package com.ptl.ddt.ptlframework.configuration;

import com.ptl.ddt.ptlclient.client.ProcessTemplatesClient;
import com.ptl.ddt.ptlframework.actions.TemplateExecutor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TemplateExecutorConfiguration {

    @Bean
    TemplateExecutor templateExecutor(ProcessTemplatesClient client) {
        return new TemplateExecutor(client);
    }
}