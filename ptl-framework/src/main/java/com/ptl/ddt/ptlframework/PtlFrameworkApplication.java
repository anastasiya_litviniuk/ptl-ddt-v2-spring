package com.ptl.ddt.ptlframework;

import com.ptl.ddt.ptlframework.runner.ProcessTemplateRunner;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@RequiredArgsConstructor
public class PtlFrameworkApplication implements CommandLineRunner {
    private final ProcessTemplateRunner runner;

    public static void main(String[] args) {
        SpringApplication.run(PtlFrameworkApplication.class, args).close();;
    }

    @Override
    public void run(String... args) {
        runner.runProcessTemplatesLauncher();
    }
}
