package com.ptl.ddt.ptlframework.configuration;

import com.ptl.ddt.ptlframework.configurationproperties.TeamScopeFilterProperties;
import com.ptl.ddt.ptlframework.filtering.IgnoredExtensionFilter;
import com.ptl.ddt.ptlframework.filtering.TeamScopeFilter;
import com.ptl.ddt.ptlframework.runner.ProcessTemplateRunner;
import com.ptl.ddt.ptlframework.handler.ProcessTemplateHandler;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:application.properties")
@EnableConfigurationProperties(TeamScopeFilterProperties.class)
public class ProcessTemplateRunnerConfiguration {

    @Bean
    ProcessTemplateRunner processTemplateRunner(ProcessTemplateHandler handler, TeamScopeFilter filter, IgnoredExtensionFilter ignoredExtensionFilter) {
        return new ProcessTemplateRunner(handler, filter, ignoredExtensionFilter);
    }
}
