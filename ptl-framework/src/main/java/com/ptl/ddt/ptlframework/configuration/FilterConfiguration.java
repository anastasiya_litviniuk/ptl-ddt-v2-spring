package com.ptl.ddt.ptlframework.configuration;

import com.google.gson.Gson;
import com.ptl.ddt.ptlframework.configurationproperties.TeamScopeFilterProperties;
import com.ptl.ddt.ptlframework.filtering.IgnoredExtensionFilter;
import com.ptl.ddt.ptlframework.filtering.TeamScopeFilter;
import com.ptl.ddt.ptlframework.filtering.scope.TeamScopeJson;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.io.InputStream;
import java.io.InputStreamReader;

@Configuration
@EnableConfigurationProperties(TeamScopeFilterProperties.class)
@PropertySource("classpath:application.properties")
public class FilterConfiguration {

    @Bean
    TeamScopeFilterProperties filterProperties() {
        return new TeamScopeFilterProperties();
    }

    @Bean
    IgnoredExtensionFilter ignoredExtensionFilter() {
        return new IgnoredExtensionFilter();
    }

    @SneakyThrows
    @Bean
    TeamScopeFilter teamScopeFilter(@Value("${pt.filter.team-scope}") String teamScope, @Value("${pt.filter.team-name}") String teamName) {
        Gson gson = new Gson();
        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("team-scope-config.json");
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
        TeamScopeJson teamScopeJson = gson.fromJson(inputStreamReader, TeamScopeJson.class);
        return new TeamScopeFilter(filterProperties().getTeamScope(), filterProperties().getTeamName(), teamScopeJson);
    }
}
