package com.ptl.ddt.ptlframework.filtering;

import com.ptl.ddt.ptlcommon.template.ProcessTemplate;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Log4j2
@RequiredArgsConstructor
public class IgnoredExtensionFilter {

    private List<String> getIgnoredCodes() {
        List<String> ignoredCodes = new ArrayList<>();
        try {
            ignoredCodes.addAll(Files.list(Path.of("baseline"))
                    .filter(file -> file.getFileName().toString().contains(".ignore"))
                    .map(path -> path.getFileName().toString().replaceAll(".ignore", "")).collect(Collectors.toList()));
        } catch (IOException e) {
            log.error("Baseline files were not found, ignore list is empty.");
        }
        return ignoredCodes;
    }

    public void applyFilter(List<ProcessTemplate> processTemplateList) {
        log.info("Removing '*.ignore' templates. Current number of process templates: {}", processTemplateList.size());
        getIgnoredCodes().forEach(ignoredCode ->
                processTemplateList.removeIf(processTemplate -> processTemplate.getCode().equals(ignoredCode))
        );
        log.info("Number of process templates after removal of '*.ignore' ones: {}", processTemplateList.size());
    }
}
