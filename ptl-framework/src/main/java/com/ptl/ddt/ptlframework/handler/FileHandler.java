package com.ptl.ddt.ptlframework.handler;

import com.ptl.ddt.ptlcommon.template.ProcessTemplate;
import com.ptl.ddt.ptlcommon.template.ProcessTemplateReports;
import com.ptl.ddt.ptlcommon.utils.FileNameUtils;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

@Log4j2
@AllArgsConstructor
public class FileHandler {

    private final ProcessTemplate processTemplate;
    private final String currentPath;
    private final String baselinePath;

    public void downloadFile(byte[] data, String contentType) {
        downloadFile(data, contentType, "");
    }

    public void downloadFile(byte[] data, String contentType, String prefix) {
        FileOutputStream outputStream;
        try {
            processTemplate.setContentType(contentType);
            String fileName = prefix + getFileName(processTemplate, contentType);
            Path filePath = Paths.get(currentPath, fileName);
            outputStream = new FileOutputStream(filePath.toString());
            outputStream.write(data);
            outputStream.close();
            log.info("File {} downloaded.", filePath);
            //todo subtask can result in multiple files
            processTemplate.getReportFiles().add(new ProcessTemplateReports()
                    .withCurrentFile(filePath)
                    .withBaselineFile(Paths.get(baselinePath, fileName)));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String getFileName(ProcessTemplate processTemplate, String contentType) {
        return String.format("%s.%s.%s",
                FileNameUtils.normalizeFilename(processTemplate.getType()),
                FileNameUtils.normalizeFilename(processTemplate.getCode()),
                FileNameUtils.getExtension(contentType));
    }
}
