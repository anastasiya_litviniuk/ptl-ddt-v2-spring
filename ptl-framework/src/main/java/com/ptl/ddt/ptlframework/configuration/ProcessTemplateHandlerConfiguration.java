package com.ptl.ddt.ptlframework.configuration;

import com.ptl.ddt.filecompare.diff.DiffCompare;
import com.ptl.ddt.filecompare.html.HtmlCompare;
import com.ptl.ddt.ptlclient.client.ProcessTemplatesClient;
import com.ptl.ddt.ptlframework.handler.ProcessTemplateHandler;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
public class ProcessTemplateHandlerConfiguration {

    @Bean
    public ProcessTemplateHandler processTemplateHandler(ProcessTemplatesClient client, @Value("current") String currentPath,
                                                         @Value("baseline")String baselinePath, HtmlCompare htmlCompare, DiffCompare diffCompare){
        return new ProcessTemplateHandler(client, currentPath, baselinePath, htmlCompare, diffCompare);
    }
}
