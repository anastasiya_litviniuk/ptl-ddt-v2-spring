package com.ptl.ddt.ptlframework.filtering.scope;

import lombok.Data;

@Data
public class ProcessTemplateScope {
    String functionCode;
    String type;
    String module;
    Integer priority;
    String executionType;
    String category;
    boolean fileDownloadMandatory;
}
