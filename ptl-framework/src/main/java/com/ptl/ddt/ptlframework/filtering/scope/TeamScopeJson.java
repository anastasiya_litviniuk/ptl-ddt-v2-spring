package com.ptl.ddt.ptlframework.filtering.scope;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

import java.util.List;

@Data
public class TeamScopeJson {
    List<Team> teams;
    List<Scopes> scopes;

    @Data
    public class Team {
        @SerializedName("processTemplates")
        List<ProcessTemplateScope> processTemplateScopes;
        @SerializedName("team")
        String name;
    }

    @Data
    public class Scopes {
        String level;
        List<Integer> priorities;
    }
}
