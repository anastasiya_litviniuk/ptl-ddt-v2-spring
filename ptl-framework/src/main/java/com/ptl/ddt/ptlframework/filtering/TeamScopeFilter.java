package com.ptl.ddt.ptlframework.filtering;

import com.google.gson.Gson;
import com.ptl.ddt.ptlcommon.template.ProcessTemplate;
import com.ptl.ddt.ptlframework.filtering.scope.ProcessTemplateScope;
import com.ptl.ddt.ptlframework.filtering.scope.TeamScopeJson;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Log4j2
@Getter
@RequiredArgsConstructor
public class TeamScopeFilter {

    private final String teamScope;
    private final String teamName;
    private final TeamScopeJson teamScopeJson;

    private List<Integer> getPriorities() {
        List<Integer> priorities = new ArrayList<>();
        teamScopeJson.getScopes().stream()
                .filter(scope -> scope.getLevel().equals(teamScope))
                .findFirst()
                .ifPresent(s -> priorities.addAll(s.getPriorities()));
        return priorities;
    }

    private List<ProcessTemplateScope> getProcessTemplateScopes() {
        List<ProcessTemplateScope> processTemplateScopes = new ArrayList<>();
        var teams = teamScopeJson.getTeams();
        teamScopeJson.getTeams().stream()
                .filter(team -> team.getName().equals(teamName))
                .findFirst()
                .ifPresent(t -> processTemplateScopes.addAll(t.getProcessTemplateScopes()));
        return processTemplateScopes;
    }

    public List<ProcessTemplate> getInScopeList(List<ProcessTemplate> processTemplateList) {
        var processTemplateScopes = getProcessTemplateScopes();
        List<String> typesInScope = createTypesInScope(processTemplateScopes);
        List<String> categoriesInScope = createCategoryScope(processTemplateScopes);
        return processTemplateList.stream().filter(template ->
                typesInScope.contains(stripWhiteSpaces(template.getType()))
                        && categoriesInScope.contains(stripWhiteSpaces(template.getCategory()))
        ).collect(Collectors.toList());
    }

    private List<String> createCategoryScope(List<ProcessTemplateScope> templateScopes) {
        List<String> categoriesInScope = new ArrayList<>();
        List<Integer> priorities = getPriorities();
        templateScopes.forEach(scope -> {
            if (priorities.contains(scope.getPriority())) {
                categoriesInScope.add(stripWhiteSpaces(scope.getCategory()));
            }
        });
        return categoriesInScope;
    }

    private List<String> createTypesInScope(List<ProcessTemplateScope> templateScopes) {
        List<String> typesInScope = new ArrayList<>();
        List<Integer> priorities = getPriorities();
        templateScopes.forEach(scope -> {
            if (priorities.contains(scope.getPriority())) {
                typesInScope.add(stripWhiteSpaces(scope.getType()));
            }
        });
        return typesInScope;
    }

    private static String stripWhiteSpaces(String text) {
        return text.replaceAll("\\s", "");
    }
}
